## Cluster Creation

## Pre-reqs

1. Install [kind](https://kind.sigs.k8s.io)
2. Install [Helm V3](https://helm.sh)

## Instructions

2. Run `make dev-up`
- This will create the kind cluster, run cluster bootstrapping, and deploy the server application.
- It takes a couple minutes for the metrics server to fully start. 

3. Once the cluster is done spinning up, check to make sure the server and load-testing tool are up and running. Example below:

```
kubectl get deploy                                                                

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
server-1712586662-server   1/1     1            1           3m26s

---

kubectl get cronjobs    

NAME                                  SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
loadtests-1712586821-vegeta-cronjob   */1 * * * *   True      0        <none>          93s
```
4. To start the load tests and what the pods scale up in action, run the following commands in seperate shells:

```
kubectl get po -w
```

```
kubectl create job --from=cronjob/loadtests-vegeta-cronjob load-test-04-08-2024-1
```

```
# you can use this to track the cpu/memory usage of the pod
kubectl top pod
```

5. After we have verified that the load testing work properly, if we want to tear down the cluster we can run `make cluster-teardown`

## Diagram

![alt text](/images/integral-overview.png)
