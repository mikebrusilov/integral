package main

import (
	"fmt"
	"net/http"
)

func fibonacci(n int) int {
	if n <= 1 {
		return n
	}
	return fibonacci(n-1) + fibonacci(n-2)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// random compute operation
	fibonacci(40)
	fmt.Fprintf(w, "Hello, you've requested: %s\n", r.URL.Path)

}

func main() {
	http.HandleFunc("/", handler)
	fmt.Println("Starting server at port 8080")
	http.ListenAndServe(":8080", nil)
}
