#!/bin/bash

HELM_MAJOR_VERSION=$(helm version --short | cut -d. -f1)

export HELM_MAJOR_VERSION=$HELM_MAJOR_VERSION

echo "export HELM_MAJOR_VERSION=$HELM_MAJOR_VERSION"