# In this case we are running a makefile command to spin up a local cluster
# A better way to create the cluster would be via IAC (terraform, pulumi) or using the operator framework.
dev-up: create-cluster cluster-plumbing metrics-server-patch deploy-services

cluster-teardown: 
	kind delete clusters mkbru-cluster

create-cluster:
	kind create cluster --name mkbru-cluster --image kindest/node:v1.26.14

# this would have been better in the tooling, was taking to long to add in the tooling so added it here
# the metrics server in the cluster will start running about a minute after this patch is deployed
# it would have been easier to deploy the metrics server via helm
# workaround for k8s using self-signed certs for kubelets and metric-server doesn't like that
metrics-server-patch:
	kubectl patch -n kube-system deployment metrics-server --type=json \
      -p '[{"op":"add","path":"/spec/template/spec/containers/0/args/-","value":"--kubelet-insecure-tls"}]'

deploy-services: helm-version deploy-charts

deploy-charts:
	helm install server ./charts/server
	helm install loadtests ./charts/loadtests 

# would be better to keep this binary in in artifactory like JFROG, this is for local testing only
cluster-plumbing:
	cd tooling && ./plumbing

build-server-image:
	docker build -t server-mkbru server/

tag-server-image:
	docker tag server-mkbru:latest mkbru/server-mkbru:latest

# login required to run this step
push-server-image:
	docker push mkbru/server-mkbru:latest

build-loadtests-image:
	docker build -t vegeta-loadtests loadtests/

tag-loadtests-image:
	docker tag vegeta-loadtests:latest mkbru/vegeta-loadtests:latest

# login required to run this step
push-loadtests-image:
	docker push mkbru/vegeta-loadtests:latest

build-cluster-plumbing-image:
	docker build -t cluster-plumbing tooling/

tag-cluster-plumbing-image:
	docker tag cluster-plumbing:latest mkbru/cluster-plumbing:latest

# login required to run this step
push-cluster-plumbing-image:
	docker push mkbru/cluster-plumbing:latest

# making sure we are on v3
helm-version:
	$(export_helm_version)

define export_helm_version
	echo -n "INFO: Checking helm version required: " && \
	eval $$($(PWD)/scripts/export_helm_version.sh ${*}) && \
    echo $${HELM_MAJOR_VERSION} | grep -q "^v3$$" || (echo "HELM_MAJOR_VERSION is not v3"; exit 1)
endef