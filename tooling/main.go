package main

import (
	"fmt"
	"mkbru/integral/tooling/k8s"
	"os"
)

func run() error {
	if err := k8s.ApplyYAMLs(); err != nil {
		return err
	}

	if err := k8s.AddMetricsServer(); err != nil {
		return err
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}
