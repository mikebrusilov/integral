package k8s

import (
	"os"
	"os/exec"
)

func ApplyYAMLs() error {
	kubectlRun := exec.Command("kubectl", "apply", "-f", "./yamls")
	kubectlRun.Stdin = os.Stdin
	kubectlRun.Stdout = os.Stdout
	kubectlRun.Stderr = os.Stderr

	if err := kubectlRun.Run(); err != nil {
		return err
	}

	return nil
}

func AddMetricsServer() error {
	kubectlRun := exec.Command("kubectl", "apply", "-f", "https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.7.1/components.yaml")
	kubectlRun.Stdin = os.Stdin
	kubectlRun.Stdout = os.Stdout
	kubectlRun.Stderr = os.Stderr

	if err := kubectlRun.Run(); err != nil {
		return err
	}

	// time.Sleep(30 * time.Second)

	// kubectlRunPatch := exec.Command(
	// 	"kubectl",
	// 	"patch",
	// 	"-n",
	// 	"kube-system",
	// 	"deployment",
	// 	"metrics-server",
	// 	"--type=json",
	// 	"-p",
	// 	"[{\"op\":\"add\",\"path\":\"/spec/template/spec/containers/0/args/-\",\"value\":\"--kubelet-insecure-tls\"}]",
	// )
	// kubectlRunPatch.Stdin = os.Stdin
	// kubectlRunPatch.Stdout = os.Stdout
	// kubectlRunPatch.Stderr = os.Stderr

	return nil
}
